const path = require('path');

const Pedido = require(path.join(__dirname,'..','models','Pedido'));
const Conn = require(path.join(__dirname, 'Connection'));

class DAOPedidos{
  static getPedidos(){
    let conn = new Conn();

    let pusher = result=>{
      return new Promise((resolve,reject)=>{
        let data = [];
        result.map(result=>{
          data.push(new Pedido(result.nombre,result.idpedido));
        });
        resolve(data);
      })
    }

    return new Promise((resolve, reject)=>{
      conn.useConn().query('CALL getPedidos()',(error,result)=>{
        conn.closeConn();
        error
        ? reject(new Error('problema CALL getPedidos()'))
        : pusher(result[0]).then(data => resolve(data));
      })
    });
  }

  static insertPedido(viaje){
    console.log(viaje.toString())
    return true;
  }

  static dropPedido(numero){
    let conn = new Conn();

    conn.useConn().query(`call delPedido(${numero})`,(error,result)=>{
      conn.closeConn();
    })
  }
}

module.exports = DAOPedidos;
