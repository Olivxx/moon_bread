const path = require('path');

const Viaje = require(path.join(__dirname,'..','models','Viaje'))
const DAOPedidos = require(path.join(__dirname,'DAOPedidos'))

class PedidosController{
  static getPedidos(){
    return new Promise((resolve,reject)=>{
      DAOPedidos.getPedidos().then(data=>{
        resolve(data)
      }).catch((error)=>{
        reject('errorGetAllPedidos')
      })
    })
  }

  static neoPedido(neoPJSON){
    let neoViaje = new Viaje(neoPJSON.id,neoPJSON.name,neoPJSON.comment);
    return (DAO.insertPedido(neoPedido));
  }

  static dropPedido(numero){
    DAOPedidos.dropPedido(numero)
  }
}

module.exports = PedidosController;
