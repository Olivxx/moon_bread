const path = require('path')

const mysql = require('mysql');
const keys = require('../settings/keys').keys

class Conn {
  connection;

  constructor() {
    this.connection = this.createConn();
  }

  createConn(){
    const connection = mysql.createConnection(keys);
    return connection;
  }

  establishConn(){
    this.connection.connect(function(error){
     if(error){
       console.log('problema en Conexion');
        return false;
     }else{
        console.log('Conexion correcta.');
     }
    });
    return true;
  }

  useConn(){
    return this.connection;
  }

  closeConn(){
    this.connection.end();
    return true;
  }
}

module.exports = Conn;
