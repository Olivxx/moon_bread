module.exports = {
  entry: {
    home: './client/home.js',
    ask: './client/ask.js',
    admin: './client/admin.js'
  },
  output: {
    path: __dirname + '/public/bundles',
    filename: '[name].bundle.js'
  }
};
