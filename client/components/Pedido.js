import React from 'react'
import ReactDOM from 'react-dom'
import FilaPedida from './FilaPedida'

class Pedido extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.props.pedido

    this.theDelete = this.theDelete.bind(this);
    this.theChange = this.theChange.bind(this);
  }

  sendMe(){
    fetch('/api/genPedido', {
      method: 'POST',
      body: JSON.stringify({
          cliente: this.props.cliente,
          pedido:this.state.products,
          total: this.comprobarTotal()
        }),
      headers:{
          'Content-Type': 'application/json'
        }
      }
    )
    console.log(this.state.products)
    this.setState(
      {
        products:[]
      }
    )
  }

  comprobarTotal(){
    let total = 0;
    this.state.products.map((producto=>
      total = producto.sub + total
    ));
    return total;
  }

  theChange(pro,can,unit){
    this.setState(function(state,props){
      let aux = state.products;

      for (let i = 0; i < aux.length; i++){
        if(aux[i].producto == pro){
          aux[i].cantidad = can;
          aux[i].sub = unit * (can || 1);
          break;
        }
      }

      return {
        products: aux,
        total: this.comprobarTotal()
      };
    });
    this.props.volatil(this.state)
  }

  theDelete(param){
    this.setState(function(state,props){
      let aux = state.products;

      for (let i = 0; i < aux.length; i++){
        if(aux[i].producto == param){
          aux.splice(i,1);
          break;
        }
      }
      return {
        products: aux,
        total: this.comprobarTotal()
      };
    });
    this.props.volatil(this.state)
  }

  render(){
    return(
      React.createElement("div",{id:'pedido'},
        React.createElement("div",{className:'subcont section'},
          React.createElement("table",{className:'centered tableop'},
            React.createElement("thead",null,
              React.createElement("tr",null,
                React.createElement("th",null,'Producto'),
                React.createElement("th",null,'Precio unidad'),
                React.createElement("th",null,'Cantidad (Min 1)'),
                React.createElement("th",null,'Subtotal'),
                React.createElement("th",null,'')
              )
            ),
            React.createElement("tbody",null,
              this.state.products.map((producto)=>
                React.createElement(FilaPedida,{producto:producto.producto, precioUnidad:producto.precioUnidad,
                  sub: producto.sub, cantidad: producto.cantidad, delete:this.theDelete, change:this.theChange,
                  key:producto.producto})
              )
            )
          ),
          React.createElement("div",{className:'row section endbar'},
            React.createElement("div",{className:'row col s5 offset-s1 total center-align'},
              `Total: $${this.comprobarTotal()}`
            ),
            React.createElement("a",{onClick: this.sendMe.bind(this), className:'waves-effect waves-light btn col s4 offset-s1 brown lighten-2 registrame'},'¡COMPRAR!',
              React.createElement("i",{className:'material-icons left'},'filter_drama'),
              React.createElement("i",{className:'material-icons right'},'filter_drama')
            )
          )
        )
      )
    )
  }
}

export default Pedido;
