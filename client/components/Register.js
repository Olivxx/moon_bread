import React from 'react'
import ReactDOM from 'react-dom'

import Login from './Login.js'

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {change: false}

    this.theChange = this.theChange.bind(this);
  }

  theChange(){
    this.setState({change:true})
  }

  handleSubmit(e){
    e.preventDefault();
    fetch('api/newUser', {
      method: 'POST',
      body: JSON.stringify({
        perro: 'jijueputa'
      }),
      headers:{
        'Content-Type': 'application/json'
      }
    });
  }

  comprobar(){
    console.log(document.getElementById('representante').checked);
    if(document.getElementById('representante').checked){
      document.getElementById('empresa').disabled = false;
      document.getElementById('empresaName').innerText = 'Ingrese el nombre de la empresa';
    }
    else{
      document.getElementById('empresa').disabled = true;
      document.getElementById('empresaName').innerText = 'Campo solo para representantes de empresas';
    }
  }

  render(){
    let register =
      React.createElement("div",{className:'card', id:'Register'},
        React.createElement("div",{className:'card-content'},
          React.createElement("div",{className:'card-title'},
            "Regístrate"
          ),
          React.createElement("div",{className:'card-content'},
            React.createElement("form",{name:'newUser', onSubmit:this.handleSubmit},
              React.createElement("div",{className:'row formulario'},
                React.createElement("div",{className:'input-field col s6 campo'},
                  React.createElement("input",{name: 'nombre', id: 'nombre',className:'validate', type:'text'}),
                  React.createElement("label",{htmlFor: 'nombre'}, 'Nombre')
                ),
                React.createElement("div",{className:'input-field col s6 campo'},
                  React.createElement("input",{name: 'apellidos', id: 'apellidos',className:'validate', type:'text'}),
                  React.createElement("label",{htmlFor: 'apellidos'}, 'Apellidos')
                ),
                React.createElement("div",{className:'input-field col s12 campo'},
                  React.createElement("input",{name: 'correo', id: 'correo',className:'validate', type:'email'}),
                  React.createElement("label",{htmlFor: 'correo'}, 'Correo Electrónico'),
                  React.createElement("span",{className:'helper-text'},'El que usarás para iniciar sesión.')
                ),
                React.createElement("div",{className:'input-field col s6 campo'},
                  React.createElement("input",{name: 'password1', id: 'password1',className:'validate', type:'password'}),
                  React.createElement("label",{htmlFor: 'password1'}, 'Contraseña')
                ),
                React.createElement("div",{className:'input-field col s6 campo'},
                  React.createElement("input",{name: 'password2', id: 'password2',className:'validate', type:'password'}),
                  React.createElement("label",{htmlFor: 'password2'}, 'Confirma la contraseña')
                ),
                React.createElement("label",{className:'input-field col s12 campo'},
                  React.createElement("input",{onClick: this.comprobar,name: 'representante', id: 'representante', type:'checkbox'}),
                  React.createElement("span",{htmlFor: 'representante'}, 'Represento a una empresa')
                ),
                React.createElement("div",{className:'input-field col s12 campo'},
                  React.createElement("input",{disabled:true, name: 'empresa', id: 'empresa', className:'validate',type:'text'}),
                  React.createElement("label",{id:'empresaName', htmlFor: 'empresa'}, 'Campo para representantes de empresas')
                ),
                React.createElement("div",{className:'input-field col s8 campo'},
                  React.createElement("input",{name: 'direccion', id: 'direccion',className:'validate', type:'text'}),
                  React.createElement("label",{htmlFor: 'direccion'}, 'Dirección'),
                  React.createElement("span",{className:'helper-text'},'A este lugar enviaremos tus pedidos.')
                ),
                React.createElement("div",{className:'input-field col s4 campo'},
                  React.createElement("input",{name: 'telefono', id: 'telefono',className:'validate', type:'text'}),
                  React.createElement("label",{htmlFor: 'telefono'}, 'Teléfono')
                ),
                React.createElement("button",{type:'submit',className:'waves-effect waves-light btn col s6 offset-s3 brown lighten-2 registrame'},'Registrarme',
                  React.createElement("i",{className:'material-icons left'},'filter_drama'),
                  React.createElement("i",{className:'material-icons right'},'filter_drama')
                )
              )
            ),
            React.createElement("div",{className:'row'},
              React.createElement("a",{className:'grey-text lighten-1 center-align col s10 offset-s1 submsg', onClick:this.theChange},'¿Ya tienes cuenta? Inicia Sesión')
            )
          )
        )
      )

    let login = React.createElement(Login);
    return(
      this.state.change
      ? login
      : register
    )
  }
}

export default Register;
