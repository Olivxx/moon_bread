import React from 'react'
import ReactDOM from 'react-dom'

import Register from './Register.js'

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {change: false}

    this.theChange = this.theChange.bind(this);
  }

  handleSubmit(e){
    fetch('api/logUser', {
      method: 'POST',
      body: JSON.stringify({
        correo: document.getElementById('correo').value,
        contrasenia: document.getElementById('password1').value,
      }),
      headers:{
        'Content-Type': 'application/json'
      }
    });
  }

  theChange(){
    this.setState({change:true})
  }

  render(){
    let login =
      React.createElement("div",{className:'card', id:'login'},
        React.createElement("div",{className:'card-content'},
          React.createElement("div",{className:'card-title'},
            "Inicia Sesión"
          ),
          React.createElement("div",{className:'card-content'},
            React.createElement("form",{submit:'/HacerPedido',className:'credenciales'},
              React.createElement("div",{className:'row'},
                React.createElement("div",{className:'input-field col s9 offset-s1 campo'},
                  React.createElement("input",{name: 'correo', id: 'correo',className:'validate', type:'email'}),
                  React.createElement("label",{htmlFor: 'correo'}, 'Correo Electrónico'),
                ),
                React.createElement("div",{className:'input-field col s9 offset-s1 campo'},
                  React.createElement("input",{name: 'password1', id: 'password1',className:'validate', type:'password'}),
                  React.createElement("label",{htmlFor: 'password1'}, 'Contraseña')
                ),
                React.createElement("a",{onClick: this.handleSubmit, className:'waves-effect waves-light btn col s6 offset-s3 brown lighten-2 registrame'},'Iniciar Sesión',
                  React.createElement("i",{className:'material-icons left'},'filter_drama'),
                  React.createElement("i",{className:'material-icons right'},'filter_drama')
                ),
              )
            ),
            React.createElement("div",{className:'row'},
              React.createElement("p",{className:'center-align xl12'},'Ó'),
              React.createElement("a",{className:'waves-effect waves-light btn col s6 offset-s3 brown lighten-2 registrame'},'Regístrate',
                React.createElement("i",{className:'material-icons left'},'filter_drama'),
                React.createElement("i",{className:'material-icons right'},'filter_drama')
              )
            )
          )
        )
      )
    let register = React.createElement(Register)

    return(
      this.state.change
      ? register
      : login
    )
  }
}

export default Login;
