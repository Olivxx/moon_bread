import React from 'react'
import ReactDOM from 'react-dom'

class ProductCard extends React.Component {
  sendMe(){
    this.props.newPro(
      {
        producto:this.props.product,
        precioUnidad:this.props.precioUnidad,
        cantidad: 1,
        sub:this.props.precioUnidad
      }
    )
  }

  render(){
    console.log('dir: ', __dirname)
    return(
      React.createElement("div", {className: 'carousel-item'},
        React.createElement("div",null,
          React.createElement("div", {className: 'card large productCard'},
            React.createElement("div",{className:'card-image'},
              React.createElement("img", {src: this.props.thumbnail}),
              React.createElement("h1", {className: 'card-title flow-text'},this.props.product),
            ),
            React.createElement("div", {className: 'card-content left-align'},
              React.createElement("p",{className: 'p'},this.props.description)
            ),
            React.createElement("div", {className: 'card-action cardAc'},
              React.createElement("div",{className:'row cardAc'},
                React.createElement("p",{className: 'label dinero col s4'},`$${this.props.precioUnidad}`),
                React.createElement("a", {onClick:this.sendMe.bind(this), className: 'black-text flow-text noPadMar right-align submsg col s6 offset-s2'},'Lo quiero!')
              )
            )
          )
        )
      )
    )
  }
}

export default ProductCard;
