import React from 'react'
import ReactDOM from 'react-dom'
import FilaPedidos from './FilaPedidos'
import Avr from './materialize.js'

class Pendientes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {data: []}
    this.tick();
  }

  componentDidMount() {
    this.timerID = setInterval(
    () => this.tick(),
    200);
  }

  tick() {
  let hasChanged;
  fetch('/api/getPedidos', {method: 'post'})
    .then((response) => {
      return response.json()
    })
    .then((productos) => {
      console.log(productos.data, 'lul')
      this.setState({data: productos.data});
    })
  }

  render() {
    if(this.state.data.length == 0)
      return React.createElement("div")
    else{
      return (
        React.createElement("div", null,
          React.createElement(FilaPedidos,{pedidos:this.state.data,actDetalles:this.props.actDetalles})
        )
      )
    }
  }
}

export default Pendientes;
