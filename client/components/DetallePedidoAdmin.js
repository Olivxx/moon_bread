import React from 'react'
import ReactDOM from 'react-dom'
import FilaPedidaAdmin from './FilaPedidaAdmin'

class DetallePedidoAdmin extends React.Component {
  completo(){
    fetch('/api/dropPedido', {
      method: 'POST',
      body: JSON.stringify({
          numero:this.props.numero
        }),
      headers:{
          'Content-Type': 'application/json'
        }
      }
    )
    this.props.cleaner()
  }

  render(){
    console.log('ochenta')
    console.log(this.props.pedido)
    return(
      React.createElement("div",{id:'pedido'},
        React.createElement("div",{className:'subcont section'},
          React.createElement("table",{className:'centered tableop'},
            React.createElement("thead",null,
              React.createElement("tr",null,
                React.createElement("th",null,'Producto'),
                React.createElement("th",null,'Cantidad')
              )
            ),
            React.createElement("tbody",null,
              this.props.pedido.map((producto)=>
                React.createElement(FilaPedidaAdmin,{producto:producto.producto, cantidad: producto.cantidad, key:producto.producto})
              )
            )
          ),
          React.createElement("div",{className:'row section endbar'},
            React.createElement("a",{onClick:this.completo.bind(this),className:'waves-effect waves-light btn col s8 offset-s2 teal darken-3'},'Pedido Listo',
              React.createElement("i",{className:'material-icons left'},'filter_drama'),
              React.createElement("i",{className:'material-icons right'},'filter_drama')
            )
          )
        )
      )
    )
  }
}

export default DetallePedidoAdmin;
