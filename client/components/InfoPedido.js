import React from 'react'
import ReactDOM from 'react-dom'
import DetalleClienteAdmin from './DetalleClienteAdmin'
import DetallePedidoAdmin from './DetallePedidoAdmin'
import Avr from './materialize.js'

class InfoPedido extends React.Component {
  render() {
    return (
      React.createElement("div", {className: 'admin', id:'admin'},
        React.createElement("div", {className: 'row'},
          React.createElement("div", {className: 'col l7 noPadding'},
            React.createElement(DetalleClienteAdmin,{cliente:this.props.cliente})
          ),
          React.createElement("div", {className: 'col l5 noPadding'},
            React.createElement(DetallePedidoAdmin,{pedido:this.props.pedido, numero:this.props.numero,cleaner:this.props.cleaner})
          )
        )
      )
    )
  }
}

export default InfoPedido;
