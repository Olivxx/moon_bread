import React from 'react'
import ReactDOM from 'react-dom'

class PendienteCard extends React.Component {
  sendMe(){
    this.props.actDetalles(
      {
        numero:this.props.numero
      }
    )
  }

  render(){
    console.log('dir: ', __dirname)
    return(
      React.createElement("div", {className: 'itemPedidos col s5 offset-s1 '},
        React.createElement("div",null,
          React.createElement("div", {className: 'card large productCard'},
          React.createElement("h1", {className: 'card-title flow-text pedidoTitle black-text'},'Nuevo Pedido'),

            React.createElement("div", {className: 'card-content left-align cContent'},
              React.createElement("div",{className:'row ccContent'},
                React.createElement("div",{className:'col s5 datosInfoPedi'},
                  React.createElement("h6",{className:'grey-text lighten-5 datosPedi'}, 'Cliente: ')
                ),
                React.createElement("div",{className:'col s7 datosPedi'},
                  React.createElement("p",{className:'datosPedi black-text'},this.props.cliente)
                ),
                React.createElement("div",{className:'col s5 datosInfoPedi'},
                  React.createElement("h6",{className:'grey-text lighten-5 datosPedi'}, 'N° Pedido: ')
                ),
                React.createElement("div",{className:'col s7 datosPedi'},
                  React.createElement("p",{className:'datosPedi black-text'},this.props.numero)
                )
              )
            ),
            React.createElement("div", {className: 'card-action cardAcPen'},
              React.createElement("div",{className:'row cardAc'},
                React.createElement("a",{onClick:this.sendMe.bind(this), className:'waves-effect waves-light btn col s6 offset-s3 deep-orange accent-3 registrame'},'Atender',
                  React.createElement("i",{className:'material-icons left'},'filter_drama'),
                  React.createElement("i",{className:'material-icons right'},'filter_drama')
                )
              )
            )
          )
        )
      )
    )
  }
}

export default PendienteCard;
