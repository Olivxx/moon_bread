import React from 'react'
import ReactDOM from 'react-dom'

class InfoMoonBread extends React.Component {
  constructor(props) {
    super(props);
    this.state = {change: false}
  }

  render(){
    let fecha = new Date();
    console.log(fecha)
    let represeta = this.props.datosCliente.empresa;

    let campoRepresenta =
      represeta
      ? React.createElement("div",{className:'row col s12 datos'},
          React.createElement("div",{className:'col s4 datosInfo'},
            React.createElement("h6",{className:'grey-text lighten-5 datos'}, 'Tu empresa: ')
          ),
          React.createElement("div",{className:'col s8 datos'},
            React.createElement("p",{className:'datos black-text'},represeta)
          ),
        )
      : React.createElement("h6",{className:'grey-text lighten-5 datos'}, 'No representas a ninguna empresa')


    return(
      React.createElement("div",{className:'card', id:'infoCliente'},
        React.createElement("div",{className:'card-content'},
          React.createElement("div",{className:'card-title'},
            "Información"
          ),
          React.createElement("div",{className:'card-content afuera'},
            React.createElement("div",{className:'row'},
              React.createElement("div",{className:'col s4 datosInfo'},
                React.createElement("h6",{className:'grey-text lighten-5 datos'}, 'Tu nombre: ')
              ),
              React.createElement("div",{className:'col s8 datos'},
                React.createElement("p",{className:'datos'},this.props.datosCliente.nombre)
              ),
              React.createElement("div",{className:'col s4 datosInfo'},
                React.createElement("h6",{className:'grey-text lighten-5 datos'}, 'Tu apellido: ')
              ),
              React.createElement("div",{className:'col s8 datos'},
                React.createElement("p",{className:'datos'},this.props.datosCliente.apellido)
              ),
              React.createElement("div",{className:'col s4 datosInfo'},
                React.createElement("h6",{className:'grey-text lighten-5 datos'}, 'Tu correo: ')
              ),
              React.createElement("div",{className:'col s8 datos'},
                React.createElement("p",{className:'datos'},this.props.datosCliente.correo)
              ),
              React.createElement("div",{className:'col s4 datosInfo'},
                React.createElement("h6",{className:'grey-text lighten-5 datos'}, 'Tu teléfono: ')
              ),
              React.createElement("div",{className:'col s8 datos'},
                React.createElement("p",{className:'datos'},this.props.datosCliente.telefono)
              ),
              React.createElement("div",{className:'col s4 datosInfo'},
                React.createElement("h6",{className:'grey-text lighten-5 datos'}, 'Tu dirección: ')
              ),
              React.createElement("div",{className:'col s8 datos'},
                React.createElement("p",{className:'datos'},this.props.datosCliente.direccion)
              ),
              campoRepresenta,
              React.createElement("div",{className:'col s4 datosInfo'},
                React.createElement("h6",{className:'grey-text lighten-5 datos'}, 'Fecha: ')
              ),
              React.createElement("div",{className:'col s8 datos'},
                React.createElement("p",{className:'datos'},this.props.datosCliente.fecha)
              )
            ),
            React.createElement("div",{className:'row'},
              React.createElement("a",{className:'left-align grey-text lighten-1 col s10 submsg', onClick:this.theChange},'Cerrar sesión.')
            )
          )
        )
      )
    )
  }
}

export default InfoMoonBread;
