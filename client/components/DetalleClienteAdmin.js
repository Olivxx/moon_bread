import React from 'react'
import ReactDOM from 'react-dom'

class DetalleClienteAdmin extends React.Component {
  render(){
    let fecha = new Date();
    let represeta = (this.props.cliente.representa||'');

    let campoRepresenta =
      represeta
      ? React.createElement("div",{className:'row col s12 datos'},
          React.createElement("div",{className:'col s4 datosInfo'},
            React.createElement("h6",{className:'grey-text lighten-5 datos'}, 'Tu empresa: ')
          ),
          React.createElement("div",{className:'col s8 datos'},
            React.createElement("p",{className:'datos black-text'},represeta)
          ),
        )
      : React.createElement("h6",{className:'grey-text lighten-5 datos'}, 'No representa a ninguna empresa')


    return(
      React.createElement("div",{className:'card', id:'infoCliente'},
        React.createElement("div",{className:'card-content'},
          React.createElement("div",{className:'card-title'},
            "Cliente"
          ),
          React.createElement("div",{className:'card-content afuera'},
            React.createElement("div",{className:'row'},
              React.createElement("div",{className:'col s4 datosInfo'},
                React.createElement("h6",{className:'grey-text lighten-5 datos'}, 'Nombre: ')
              ),
              React.createElement("div",{className:'col s8 datos'},
                React.createElement("p",{className:'datos'},(this.props.cliente.nombre || 'Selecciona un pedido'))
              ),
              React.createElement("div",{className:'col s4 datosInfo'},
                React.createElement("h6",{className:'grey-text lighten-5 datos'}, 'Apellido: ')
              ),
              React.createElement("div",{className:'col s8 datos'},
                React.createElement("p",{className:'datos'},(this.props.cliente.apellido || 'Selecciona un pedido'))
              ),
              React.createElement("div",{className:'col s4 datosInfo'},
                React.createElement("h6",{className:'grey-text lighten-5 datos'}, 'Correo: ')
              ),
              React.createElement("div",{className:'col s8 datos'},
                React.createElement("p",{className:'datos'},(this.props.cliente.correo || 'Selecciona un pedido'))
              ),
              React.createElement("div",{className:'col s4 datosInfo'},
                React.createElement("h6",{className:'grey-text lighten-5 datos'}, 'Teléfono: ')
              ),
              React.createElement("div",{className:'col s8 datos'},
                React.createElement("p",{className:'datos'},(this.props.cliente.telefono || 'Selecciona un pedido'))
              ),
              React.createElement("div",{className:'col s4 datosInfo'},
                React.createElement("h6",{className:'grey-text lighten-5 datos'}, 'Dirección: ')
              ),
              React.createElement("div",{className:'col s8 datos'},
                React.createElement("p",{className:'datos'},(this.props.cliente.direccion || 'Selecciona un pedido'))
              ),
              campoRepresenta,
              React.createElement("div",{className:'col s4 datosInfo'},
                React.createElement("h6",{className:'grey-text lighten-5 datos'}, 'Fecha: ')
              ),
              React.createElement("div",{className:'col s8 datos'},
                React.createElement("p",{className:'datos'},(this.props.cliente.fechaPedido || 'Selecciona un pedido'))
              )
            )
          )
        )
      )
    )
  }
}

export default DetalleClienteAdmin;
