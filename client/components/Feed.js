import React from 'react'
import ReactDOM from 'react-dom'
import Carrusel from './Carrusel.js'
import Avr from './materialize.js'

class Feed extends React.Component {
  constructor(props) {
    super(props);
    this.state = {data: []}
    this.tick();
  }

  componentDidMount() {
    this.timerID = setInterval(
    () => this.tick(),
    3000);
  }

  equal(data1,data2){
    if(data1.length == data2.length){
      for(let i = 0; i < data1.length ; i++)
        if(!(data1[i].producto == data2[i].producto) || !(data1[i].thumbnail == data2[i].thumbnail) || !(data1[i].precioUnidad == data2[i].precioUnidad))
          return true
      return false
    }else
      return true
  }

  tick() {
    let hasChanged;
    fetch('/api/getProductos', {method: 'post'})
      .then((response) => {
        return response.json()
      })
      .then((productos) => {
        hasChanged = this.equal(this.state.data,productos.data)
        if(hasChanged)
          this.setState({data: productos.data});
      }).then(()=>{
        if(hasChanged){
          var elemis = document.querySelectorAll('.carousel');
          var instances = M.Carousel.init(elemis,
          {
            duration:90,
            numVisible: 16,
            dist: -80,
            padding: 60,
            shift: 80
          })
          instances[0].next(5);
        }
    })
  }

  render() {
    if(this.state.data.length == 0)
      return React.createElement("div")
    else
      return React.createElement(Carrusel, {data: this.state.data,newPro:this.props.newPro})
  }
}

export default Feed;
