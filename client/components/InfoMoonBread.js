import React from 'react'
import ReactDOM from 'react-dom'

class InfoMoonBread extends React.Component {
  render(){
    return(
      React.createElement("div",{id:'infoMoonBread'},
        React.createElement("div",{id:'infoText'},
          React.createElement("h3",{className:'grey-text lighten-5'}, 'Bienvenido a Moon Bread'),
          React.createElement("br"),
          React.createElement("p",{className:'white-text'},'Los mejores productos de panadería artesanales, hechos con el mayor cuidado y apreciación por el buen gusto.'),
          React.createElement("p",{className:'white-text'},'Inicia sesión o regístrate para disfutar de esta experiencia y tener la más alta calidad en tu negocio o en la puerta de tu casa')
        )
      )
    )
  }
}

export default InfoMoonBread;
