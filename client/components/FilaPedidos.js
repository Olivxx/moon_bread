import React from 'react'
import ReactDOM from 'react-dom'
import PendienteCard from './PendienteCard.js'

class FilaPedidos extends React.Component {
  render(){
    return(
      React.createElement("table",{className:'centered tablePedidos'},
        React.createElement("tbody",{className:'bodytable'},
          React.createElement("tr",null,
            React.createElement("td",{className:'letd'},this.props.nombre,
              React.createElement("div",{className:'row lecountain'},
                this.props.pedidos.map((pedido)=>
                  React.createElement(PendienteCard,{cliente:pedido.cliente, numero:pedido.idpedido,actDetalles:this.props.actDetalles,key:pedido.idpedido})
                )
              )
            )
          )
        )
      )
    )
  }
}

export default FilaPedidos;
