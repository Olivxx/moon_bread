import React from 'react'
import ReactDOM from 'react-dom'

class FilaPedida extends React.Component {
  detele(){
    this.props.delete(this.props.producto)
  }

  sumador(e){
    const {value} = e.target;
    if(!isNaN(value) && value < 1001){
      this.props.change(this.props.producto,value,this.props.precioUnidad)
    }
  }

  render(){
    return(
      React.createElement("tr",null,
        React.createElement("td",null,this.props.producto),
        React.createElement("td",null,this.props.precioUnidad),
        React.createElement("td",null,
          React.createElement("input",{type:'text', className:'center-align cantidad',onChange:this.sumador.bind(this), id:'cantidad',value:this.props.cantidad})
        ),
        React.createElement("td",null,this.props.sub),
        React.createElement("td",null,
          React.createElement("a",{className:'btn-floating btn-small waves-effect waves-light red darken-3',onClick:this.detele.bind(this)},'X')
        )
      )
    )
  }
}

export default FilaPedida;
