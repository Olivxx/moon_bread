import React from 'react'
import ReactDOM from 'react-dom'
import ProductCard from './ProductCard.js'

class Carrusel extends React.Component {
  render(){
    console.log(this.props.data.length)
    return(
      React.createElement("div",{className:'row leCarrus'},
        React.createElement("div", {className: 'carousel col s12 leCarrus'},
          this.props.data.map((data)=>
            React.createElement(ProductCard, {product:data.product, description:data.description, precioUnidad:data.precioUnidad, thumbnail: data.thumbnail, key:data.product,newPro:this.props.newPro})
          )
        )
      )
    )
  }
}

export default Carrusel;
