import React from 'react'
import ReactDOM from 'react-dom'

class FilaPedidaAdmin extends React.Component {
  render(){
    return(
      React.createElement("tr",null,
        React.createElement("td",null,this.props.producto),
        React.createElement("td",null,this.props.cantidad)
      )
    )
  }
}

export default FilaPedidaAdmin;
