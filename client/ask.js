import React from 'react'
import ReactDOM from 'react-dom'
import InfoCliente from './components/InfoCliente.js'
import Pedido from './components/Pedido'
import Feed from './components/Feed'
import Avr from './components/materialize.js'

class Ask extends React.Component {
  constructor(props) {
    super(props);
    this.volatile = {
      products:[],
      total: 0
    }
    this.state = {
      pedido:this.volatile,
      datosCliente:{}
    }
    this.theVolatile = this.theVolatile.bind(this);
    this.theNewPro = this.theNewPro.bind(this);
    this.getPerfil()
  }

  getPerfil(){
    fetch('/api/getPerfil', {method: 'post',
      body:{
        correo:'mail'
      }
    }).then((response) => {
        return response.json()
    }).then((perfil) => {
      console.log(perfil)
      this.setState({
        datosCliente:perfil.data
      });
    })
  }

  theVolatile(state){
    this.volatile = state
  }

  theNewPro(neoProduct){
    let isit = false
    this.volatile.products.map((producto)=>{
      if(neoProduct.producto == producto.producto){
        producto.cantidad += 1;
        producto.sub = producto.precioUnidad * producto.cantidad
        isit = true
      }
    })
    if(!isit){
      this.volatile.products.push(neoProduct)
    }
    this.setState({pedido:this.volatile});
  }

  render() {
    return (
      React.createElement("div", {className: 'ask', id:'ask'},
        React.createElement("div", {className: 'row'},
          React.createElement("div", {className: 'col l4 offset-l1 s11 offset-s1'},
            React.createElement(InfoCliente,{datosCliente:this.state.datosCliente})
          ),
          React.createElement("div", {className: 'col l6 offset-l1 s11', id:'pedidoss'},
            React.createElement(Pedido,{pedido:this.state.pedido,volatil:this.theVolatile,cliente:this.state.datosCliente.correo})
          ),
          React.createElement("div", {className: 'col l12 s12', id:'feed'},
            React.createElement(Feed,{newPro:this.theNewPro})
          )
        )
      )
    )
  }
}

ReactDOM.render(React.createElement(Ask),document.getElementById('root'));
