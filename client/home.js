import React from 'react'
import ReactDOM from 'react-dom'
import Login from './components/Login.js'
import Register from './components/Register.js'
import InfoMoonBread from './components/InfoMoonBread.js'
import Avr from './components/materialize.js'

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {data: []}
  }

  render() {
    return (
      React.createElement("div", {className: 'home', id:'home'},
        React.createElement("div", {className: 'row'},
          React.createElement("div", {className: 'col l5 infoMoonBread s11'},
            React.createElement(InfoMoonBread)),
          React.createElement("div", {className: 'col l4 offset-l1 s10 offset-s1', id:'userChoose'},
            React.createElement(Login)
          )
        )
      )
    )
  }
}

ReactDOM.render(React.createElement(Home),document.getElementById('root'));
