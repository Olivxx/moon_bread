import React from 'react'
import ReactDOM from 'react-dom'
import InfoPedido from './components/InfoPedido.js'
import Pendientes from './components/Pendientes.js'
import Avr from './components/materialize.js'

class Admin extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      pPedido : [],
      pCliente : [],
      pNumero: ''
    }

    this.actDetalles = this.actDetalles.bind(this);
    this.finished = this.finished.bind(this);
  }

  finished(){
    this.setState({
      pPedido : [],
      pCliente : [],
      pNumero: ''
    })
  }

  actDetalles(numero){
    fetch('/api/getDetallesPedido', {method: 'post',
      body:{
        numero:numero.numero
      }
    }).then((response) => {
        return response.json()
    }).then((basura) => {
      console.log(basura.detalles)
      this.setState({
        pCliente: basura.cliente,
        pPedido :basura.detalles,
        pNumero: numero.numero
      });
    })
  }


  render() {
    console.log(this.state.pedido)
    return (
      React.createElement("div", {className: 'admin', id:'admin'},
        React.createElement("div", {className: 'row'},
          React.createElement("div", {className: 'col l6'},
            React.createElement(Pendientes,{actDetalles:this.actDetalles})
          ),
          React.createElement("div", {className: 'col l6 s10 offset-s1'},
            React.createElement(InfoPedido,{pedido:this.state.pPedido,cliente:this.state.pCliente,numero:this.state.pNumero,cleaner:this.finished})
          )
        )
      )
    )
  }
}

ReactDOM.render(React.createElement(Admin),document.getElementById('root'));
