class Pedido{
  constructor(cliente,idpedido){
    this.cliente = cliente;
    this.idpedido = idpedido;
  }
  toString(){
    return (`Cliente: ${this.cliente}, IDpedido: ${this.idpedido}`);
  }
}

module.exports = Pedido;
