class Viaje{
  #id;
  #name;
  #comment;
  #thumbnail;

  constructor(id,name,comment,thumbnail){
    this.id = id;
    this.name = name;
    this.comment = comment;
    this.thumbnail = thumbnail;
  }
  toJSON(){
    return(
    {
      id: this.id,
      name: this.name,
      comment: this.comment,
      thumbnail: this.thumbnail
    })
  }
  toString(){
    return (`ID: ${this.id}, Name: ${this.name}, Comment: ${this.comment}`);
  }
}

module.exports = Viaje;
