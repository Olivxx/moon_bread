const express = require('express');
const path = require('path');

const ask = express.Router();

ask.all('/',function(req, res){
  res.sendFile(path.join(__dirname,'..','public','ask.html'));
});

module.exports = ask;
