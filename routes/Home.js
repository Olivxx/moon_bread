const express = require('express');
const path = require('path');

const home = express.Router();

home.all('/',function(req, res){
  res.sendFile(path.join(__dirname,'..','public','home.html'));
});

module.exports = home;
