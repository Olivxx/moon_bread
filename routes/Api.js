const express = require('express');
const path = require('path');

const PedidosController = require(path.join(__dirname,'..','controllers','PedidosController'))
const api = express.Router();

// home.all('/api/newUser',function(req, res){
//   console.log(req.body)
//   res.sendFile(path.join(__dirname,'..','public','home.html'));
// });
//
// home.all('/api/logUser',function(req, res){
//   console.log(req.body)
//   res.sendFile(path.join(__dirname,'..','public','home.html'));
// });
api.post('/genPedido',function(req,res){
  console.log(req.body)
})

api.post('/logUser',function(req,res){
  console.log('redirecting..')
  res.redirect('/HacerPedido')
})

api.post('/getPedidos',function(req,res){
  PedidosController.getPedidos().then((data)=>{
    console.log(data)
    res.json({data});
  })
})

api.all('/getProductos',function(req,res){
  res.json({data: [
    {
      product:'Baguette',
      description:'Pan largo y rico. Artesanal',
      precioUnidad: 2000,
      thumbnail: '/images/bread.jpg'
    },
    {
      product:'Bruchette',
      description:'Pan redondo y también rico. Artesanal',
      precioUnidad: 5000,
      thumbnail: '/images/bread.jpg'
    },
    {
      product:'sourdough1',
      description:'Pan redondo y también rico. Artesanal',
      precioUnidad: 5000,
      thumbnail: '/images/bread.jpg'
    },
    {
      product:'sourdough2',
      description:'Pan redondo y también rico. Artesanal',
      precioUnidad: 5000,
      thumbnail: '/images/bread.jpg'
    },
    {
      product:'sourdough3',
      description:'Pan redondo y también rico. Artesanal',
      precioUnidad: 5000,
      thumbnail: '/images/bread.jpg'
    },
    {
      product:'sourdough4',
      description:'Pan redondo y también rico. Artesanal',
      precioUnidad: 5000,
      thumbnail: '/images/bread.jpg'
    },
    {
      product:'sourdough5',
      description:'Pan redondo y también rico. Artesanal',
      precioUnidad: 5000,
      thumbnail: '/images/bread.jpg'
    },
    {
      product:'Focaccia',
      description:'Pan redondo y también rico. Artesanal',
      precioUnidad: 5000,
      thumbnail: '/images/bread.jpg'
    },
    {
      product:'Focaccia1',
      description:'Pan redondo y también rico. Artesanal',
      precioUnidad: 5000,
      thumbnail: '/images/bread.jpg'
    },
    {
      product:'Focaccia2',
      description:'Pan redondo y también rico. Artesanal',
      precioUnidad: 5000,
      thumbnail: '/images/bread.jpg'
    },
    {
      product:'Focaccia3',
      description:'Pan redondo y también rico. Artesanal',
      precioUnidad: 5000,
      thumbnail: '/images/bread.jpg'
    },
    {
      product:'Focaccia4',
      description:'Pan redondo y también rico. Artesanal',
      precioUnidad: 5000,
      thumbnail: '/images/bread.jpg'
    }
  ]})
})

api.all('/getPerfil',function(req,res){
  res.json({data: {
    nombre:'Néstor Fernando',
    apellido: 'Porras Díaz',
    correo: 'nestorpodi@unisabana.edu.co',
    telefono: '317 587 5697',
    direccion: 'Cra 9 #19-137',
    empresa:'Club Colombia',
    fecha: '27 de Mayo de 2019'
  }})
})

api.post('/getDetallesPedido',function(req,res){
  console.log('pidiendo')
  res.json({
    cliente:{
      nombre:'Néstor Fernando',
      apellido:'Porras Díaz',
      correo: 'nestorpodi@unisabana.edu.co',
      telefono: '317 587 5697',
      direccion: 'Cra 9 calle 19 #19-137',
      representa: 'Club Colombia',
      fechaPedido: '27 de Mayo de 2019'
    },
    detalles:[
    {
      producto:'Popetas',
      cantidad:20
    },
    {
      producto:'Popetas2',
      cantidad:20
    },
    {
      producto:'Popetas3',
      cantidad:20
    },
    {
      producto:'Popetas4',
      cantidad:20
    },
    {
      producto:'Popetas5',
      cantidad:20
    },
    {
      producto:'Popetas6',
      cantidad:20
    },
    {
      producto:'Popetas7',
      cantidad:20
    },
    {
      producto:'Popetas8',
      cantidad:20
    },
    {
      producto:'Popetas9',
      cantidad:20
    },
    {
      producto:'Popetas10',
      cantidad:20
    }
  ]})
})

api.post('/dropPedido',function(req,res){
  PedidosController.dropPedido(req.body.numero)
})

module.exports = api;
