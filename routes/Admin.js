const express = require('express');
const path = require('path');

const admin = express.Router();

admin.all('/',function(req, res){
  res.sendFile(path.join(__dirname,'..','public','admin.html'));
});

module.exports = admin;
