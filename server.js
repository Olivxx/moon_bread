//Require core modules
const express = require('express');
const path = require('path');

//Start App
const server = express();

//reuire routes
const home = require(path.join(__dirname,'routes','Home'));
const ask = require(path.join(__dirname,'routes','Ask'));
const api = require(path.join(__dirname,'routes','Api'));
const admin = require(path.join(__dirname,'routes','Admin'));

//Configure server
server.set('port', 5000);
server.use(express.static(__dirname + '/public'));
server.use(express.json());
server.use(express.urlencoded({extended: true}));

//Establish routes
server.use('/',home);
server.use('/hacerPedido',ask);
server.use('/api',api);
server.use('/admin',admin);

//Start server
server.listen(server.get('port'),()=>{
  console.log('server listening at port ', server.get('port'));
})
